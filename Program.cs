﻿using System;

namespace PrimeNumbers
{
    class Program
    {
        static private bool CheckPrimeNumber(int number)
        {
            for (int i = 2; i <= Math.Sqrt(number); i++)
            {
                if (number % i == 0) return false;
            }
            return number!=1;
        }

        static public void GetAllPrimeNumber()
        {

            string[] numbers;
            bool side;
            string sideString;
            do
            {
                Console.WriteLine("Введите направление поиска:0-прямое направление 1-обратное напраление");
                sideString = Console.ReadLine();
            } while (sideString != "1" && sideString != "0");
            side = sideString == "0" ? false : true;
            int firstNumber = 1;
            int secondNumber = 1;
            bool result = false;
            do
            {
                Console.WriteLine("Введите одно, либо два положительных числа (через запятую)");
                numbers = Console.ReadLine().Split(",");
                if (numbers.Length == 1)
                {
                    result = int.TryParse(numbers[0].Trim(), out firstNumber) && firstNumber > 0;                
                }
                if (numbers.Length == 2)
                {
                    result = int.TryParse(numbers[0].Trim(), out firstNumber) && int.TryParse(numbers[1], out secondNumber) && firstNumber > 0 && secondNumber > 0;
                }
            }
            while (!result);
            int count = 0;
            int increment = side ? -1 : 1;
            if (numbers.Length == 1)
            {
                for (int i = firstNumber + increment; count < 3 && i > 0; i += increment)
                {
                    if (CheckPrimeNumber(i))
                    {
                        Console.WriteLine(i);
                        count++;
                    }
                }
                if (count < 3) Console.WriteLine("Таких простых чисел меньше трех");
            }

            if (numbers.Length == 2)
            {
            int start = increment == 1 ? Math.Min(firstNumber, secondNumber) + 1 : Math.Max(firstNumber, secondNumber) - 1;
            int end = increment == -1 ? Math.Min(firstNumber, secondNumber) : Math.Max(firstNumber, secondNumber);

                for (int i = start; count < 3 && increment==1?i<end:i>end; i += increment)
                {
                    if (CheckPrimeNumber(i))
                    {
                        Console.WriteLine(i);
                        count++;
                    }
                }
                if (count < 3) Console.WriteLine("Таких простых чисел меньше трех");

            }
        }
        
    




        static void Main(string[] args)
        {
            GetAllPrimeNumber();
        }

    }
}